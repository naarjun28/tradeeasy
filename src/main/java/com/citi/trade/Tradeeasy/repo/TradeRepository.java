package com.citi.trade.tradeeasy.repo;

import com.citi.trade.tradeeasy.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeRepository extends MongoRepository <Trade,ObjectId>{
 
    public Trade findById(String id);
    public void deleteById(String id);
    
}