package com.citi.trade.tradeeasy.rest;

import java.util.Collection;
import com.citi.trade.tradeeasy.entities.Trade;
import com.citi.trade.tradeeasy.entities.TradeEnum;
import com.citi.trade.tradeeasy.service.TradeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/trades")
public class TradeController {

    @Autowired
    private TradeService tservice;
    
    private static final Logger log = LoggerFactory.getLogger(TradeController.class);
    @RequestMapping(value="/getAllStocks", method = RequestMethod.GET)    
    public Collection<Trade> getAllStocks(){
        log.debug("get all stocks method");
        return tservice.getAllStocks();
    }

    @RequestMapping(value = "/getStockById/{id}", method = RequestMethod.GET)
    public Trade getStockById(@PathVariable("id") String id){
        log.debug("get stock for id "+id);
        return tservice.getStockById(id);
    }

    @RequestMapping(value = "/addStock", method = RequestMethod.POST, consumes = "application/json", produces ="application/json")
    public ResponseEntity<String> postStock(@RequestBody Trade t){
        String newStock;
        try{
        t.settEnum(TradeEnum.CREATED);
        newStock = tservice.addStock(t);
        }
        catch(Exception ex){
            log.error("error in adding new stock "+ex.getMessage());
            return new ResponseEntity<>("Exception during adding a stock"+t+"with Exception"+ex.getStackTrace(),HttpStatus.BAD_REQUEST);
        }
        log.debug("new stock added with id "+newStock);
        return new ResponseEntity<>("\"Stock Added that was posted by the admin " + newStock+"\"",HttpStatus.CREATED);
    }

    @RequestMapping(value = "/updateStockById/{id}", method = RequestMethod.PATCH,consumes = "application/json")
    public ResponseEntity<String> updateStockById(@PathVariable("id")  String id,@RequestBody Trade t)
    {
        Trade stock = getStockById(id);
        if(stock == null)
            return new ResponseEntity<>("Stock record not found for id "+id,HttpStatus.BAD_REQUEST);
        try{
            stock = tservice.updateById(id,t);
        }
        catch(Exception exception){
            return new ResponseEntity<>("Stock update failed for id "+id,HttpStatus.BAD_REQUEST);
        }
        
        log.debug("updated stock for id "+id);
        return new ResponseEntity<>("Stock updated successfully",HttpStatus.ACCEPTED);
    }
    @RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
    public void deleteStock(){
        log.debug("deleted all stocks");
        tservice.deleteAll();
    }

    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteStockById(@PathVariable("id") String id){
        Trade stock = getStockById(id);
        if(stock == null)
            return new ResponseEntity<>("Stock record not found for id "+id,HttpStatus.BAD_REQUEST);
        try{
            tservice.deleteById(id);
        }
        catch(Exception exception){
            return new ResponseEntity<>("Delete request failed with exception "+exception.getMessage(),HttpStatus.BAD_REQUEST);
        }
        log.debug("deleted stock record for id "+id);
        return new ResponseEntity<>("Delete Operation succeded for the object id "+id,HttpStatus.OK);
    }
}