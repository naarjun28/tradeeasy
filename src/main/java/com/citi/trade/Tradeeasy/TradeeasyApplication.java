package com.citi.trade.tradeeasy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeeasyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeeasyApplication.class, args);
	}

}
