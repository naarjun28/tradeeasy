package com.citi.trade.tradeeasy.service;

import java.util.Collection;
import com.citi.trade.tradeeasy.entities.Trade;
import com.citi.trade.tradeeasy.repo.TradeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository tradeRepo;

    @Override
    public String addStock(final Trade t) {
        Trade inserted = tradeRepo.insert(t);
        return inserted.getId();
    }

    @Override
    public Collection<Trade> getAllStocks() {

        return tradeRepo.findAll();
    }

    @Override
    @Transactional
    public Trade getStockById(String id) {

        return tradeRepo.findById(id);
        
    }

    @Override
    public void deleteAll() {

        tradeRepo.deleteAll();
    }

    @Override
    @Transactional
    public void deleteById(String id) {

        tradeRepo.deleteById(id);

    }

    @Override
    public Trade updateById(String id, Trade t) {
        
        Trade stock = getStockById(id);
        stock.setDate(t.getDate());
        stock.setRequestedPrice(t.getRequestedPrice());
        stock.setStockTicker(t.getStockTicker());
        stock.setStockQuantity(t.getStockQuantity());
        stock.settEnum(t.gettEnum());
        stock.setTradeType(t.getTradeType());
        stock = tradeRepo.save(stock);
        return stock;
    }

    
}