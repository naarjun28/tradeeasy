package com.citi.trade.tradeeasy.service;

import java.util.Collection;
import com.citi.trade.tradeeasy.entities.Trade;

public interface TradeService {

    String addStock(Trade t);

    Collection<Trade> getAllStocks();

    Trade getStockById(String id);

    Trade updateById(String id, Trade t);

    void deleteAll();

    void deleteById(String id);
}