package com.citi.trade.tradeeasy.entities;

public enum TradeEnum {
    CREATED,
    PROCESSING,
    CANCELLED,
    REJECTED,
    FILLED,
    PARTIALLY_FILLED,
    ERROR    
}