package com.citi.trade.tradeeasy;



import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.citi.trade.tradeeasy.entities.Trade;
import com.citi.trade.tradeeasy.entities.TradeEnum;
import com.citi.trade.tradeeasy.entities.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TradeeasyApplicationTests {

    String id = "5412568fd";
    Trade trade = new Trade();
    Date mockDate = new Date();
    double price = 5234.23;
    int quantity = 50;
    String ticker = "C";
     
    @Test
    void testTradeClass(){
       
        trade.setId(id);
        trade.setDate(mockDate);
        trade.setRequestedPrice(price);
        trade.setStockQuantity(quantity);
        trade.setStockTicker(ticker);
        trade.settEnum(TradeEnum.REJECTED);
        trade.setTradeType(TradeType.SELL);

        assertEquals(trade.getId(),id);
        assertEquals(trade.getDate(), mockDate);
        assertEquals(trade.getRequestedPrice(), price);
        assertEquals(trade.getStockQuantity(), quantity);
        assertEquals(trade.getStockTicker(), ticker);
        assertEquals(trade.gettEnum(), TradeEnum.REJECTED);
        assertEquals(trade.getTradeType(), TradeType.SELL);


    }
}
